package ua.hillel.hillelspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ua.hillel.hillelspring.configuration.MyBeanConfiguration;
import ua.hillel.hillelspring.controller.UserController;
import ua.hillel.hillelspring.entity.User;

@SpringBootApplication
public class HillelspringApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext context = SpringApplication.run(HillelspringApplication.class, args);
        final AnnotationConfigApplicationContext contextBean = new AnnotationConfigApplicationContext(MyBeanConfiguration.class);
        final String stringBean = contextBean.getBean("stringBean", String.class);
        final User customUser = contextBean.getBean("getCustomUser", User.class);

        System.out.println(stringBean);
        System.out.println(customUser);

        final UserController userController = context.getBean(UserController.class);
        userController.getAll().forEach(System.out::println);


    }

}
