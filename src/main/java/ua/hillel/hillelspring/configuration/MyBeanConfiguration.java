package ua.hillel.hillelspring.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import ua.hillel.hillelspring.entity.User;

@Configuration
@ComponentScan("ua.hillel.hillelspring.configuration")
@PropertySource("classpath:application.properties")
public class MyBeanConfiguration {

    @Value("${queue.name}")
    private String queueName;

    @Bean
    @Scope("singleton")
    public String stringBean(){
        return queueName;
    }

    @Bean
    @Scope("prototype")
    public User getCustomUser() {
        return new User("Denis", "Maksymovych", 24);
    }
}
