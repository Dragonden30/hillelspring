package ua.hillel.hillelspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ua.hillel.hillelspring.entity.User;
import ua.hillel.hillelspring.service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    public UserService userService;

    public List<User> getAll(){
        return userService.getAll();
    }
}
