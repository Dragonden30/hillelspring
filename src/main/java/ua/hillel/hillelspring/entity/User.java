package ua.hillel.hillelspring.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class User {

   // private Integer id;
    private String name;
    private String surname;
    private int age;

}
