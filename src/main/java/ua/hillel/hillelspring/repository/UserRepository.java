package ua.hillel.hillelspring.repository;

import org.springframework.stereotype.Repository;
import ua.hillel.hillelspring.entity.User;

import java.util.List;

@Repository
public class UserRepository {
    public List<User> getAll(){
        return List.of(
                new User("Denis", "Maksymovych", 24),
                new User("Max", "Bezu", 21)
        );
    }
}
